-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2019 at 09:48 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `konstan_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_job`
--

CREATE TABLE `order_job` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `jenis_order` char(200) NOT NULL,
  `waktu_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_job`
--

INSERT INTO `order_job` (`id`, `email`, `jenis_order`, `waktu_order`) VALUES
(1, '', 'konsRumahGP', '2019-06-18 04:32:35'),
(2, 'admin01@gmail.com', 'renovDapurGP', '2019-06-18 04:38:23'),
(3, 'teguh@gmail.com', 'renovDapurSP', '2019-06-18 05:19:17'),
(4, 'teguh@gmail.com', 'renovDapurPP', '2019-06-18 05:24:54'),
(5, 'admin01@gmail.com', 'renovDapurPP', '2019-06-18 11:50:01'),
(6, 'admin01@gmail.com', 'renovKamarGP', '2019-06-18 12:03:24'),
(7, 'admin01@gmail.com', 'renovDapurSP', '2019-09-11 06:15:14'),
(8, 'admin01@gmail.com', 'renovDapurGP', '2019-09-11 06:16:03'),
(9, 'yusuf123@gmail.com', 'renovDapurGP', '2019-10-15 07:45:33');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`nama`, `email`, `password`) VALUES
('aaa', 'dalsdahk', '12345678'),
('admin', 'admin01@gmail.com', 'yusufalfianto'),
('farizzzz', 'farizzz@', 'asdasd123'),
('teguh riandi', 'teguh@gmail.com', '12345678'),
('muhammad', 'yusuf123@gmail.com', 'yusuf123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_job`
--
ALTER TABLE `order_job`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_job`
--
ALTER TABLE `order_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

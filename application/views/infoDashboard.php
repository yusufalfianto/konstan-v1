<?php 
$this->load->library('session');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Konstan</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <!-- Google font -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

  <!-- Slick -->
  <link type="text/css" rel="stylesheet" href="css/slick.css"/>
  <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

  <!-- nouislider -->
  <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

  <!-- Font Awesome Icon -->
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="<?php echo base_url();?>css/style6.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/footer.css" rel="stylesheet" type="text/css">

  <!-- <link href="css/stylee.css" rel="stylesheet" type="text/css"> -->
</head>
<body>
<?php include "header.php" ?>

  <br><br><br><br><br><br>

  <!-- store products -->
  <div class="row">
    <div class="col-12" align="center">
      <!--Product1 -->
      <div class="product">
        <div class="product-img">
          <img src="<?php echo base_url();?>asset/minimalis.jpg" alt="">
          <div class="product-label">
            <span class="new">NEW</span>
          </div>
        </div>
        <div class="product-body">
          <h3 class="product-name">Rumah Minimalis</h3>
          <p class="product-category">Rumah idaman masa kini</p>
        </div>
        <div class="add-to-cart">
          <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i><a href="<?php echo base_url()?>user/rumahminim">Lihat lebih lanjut</a></button>
        </div>
      </div>
    </div>
  </div>
  <br><br><br><br><br>


  <?php include "footer.php" ?>


  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
    </body>
</html>

<?php 
$this->load->library('session');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Konstan</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <!-- Google font -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

  <!-- Slick -->
  <link type="text/css" rel="stylesheet" href="css/slick.css"/>
  <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

  <!-- nouislider -->
  <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

  <!-- Font Awesome Icon -->
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="<?php echo base_url();?>css/style6.css" rel="stylesheet" type="text/css">
  <!-- <link href="<?php echo base_url();?>css/style5.css" rel="stylesheet" type="text/css"> -->
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/footer.css" rel="stylesheet" type="text/css">
  <!-- <link href="css/stylee.css" rel="stylesheet" type="text/css"> -->
</head>
<body>
  <?php include "header.php"?>
<?php
              if($this->uri->segment(2) == "inserted")
                echo ' <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Pesanan anda telah kami terima! Cek email beberapa saat lagi</strong> .
                      </div>';     
            ?>
<div id="demo" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="<?php echo base_url();?>images/c2.jpg" alt="Los Angeles">
        <div class="carousel-caption">
          <br>
          <h3>K O N S T A N</h3>
          <p>WE BUILD WHAT YOU DREAM</p>
        </div>   
      </div>
      <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div>
  </div>

  <!-- store products -->
  <div class="product-section">
    <div class="row">
        <!--Product1 -->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/Ass1.png" alt="">
              <div class="product-label">
                <span class="new">NEW</span>
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Renovasi Dapur</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Yusuf Alfianto</h3>
              </div>
              <h4 class="product-price">Start from 22 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/renovasiDapur">Lihat lebih lanjut</a>
              </button>
            </div>
          </div>
        </div>
        <!--Product2-->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass2.png" alt="">
              <div class="product-label">
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Konstruksi Rumah</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Muhammad Fariz</h3>
              </div>
              <h4 class="product-price">Start from 110 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/KonstruksiRumah">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
        </div>
        <!--Product3-->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass4.png" alt="">
              <div class="product-label">
                <span class="new">NEW</span>
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Renovasi Kamar</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Teguh Riandi</h3>
              </div>
              <h4 class="product-price">Start from 6,5 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/renovasiKamar">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
                  <br><br><br>
        </div>

        <!--Product4-->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass4.png" alt="">
              <div class="product-label">
                <span class="new">NEW</span>
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Renovasi Ruang Keluarga</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Triyas Dharosa Putri</h3>
              </div>
              <h4 class="product-price">Start from 7 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/renovasiRuangKeluarga">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
        </div>

        <!-- Product5 -->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass2.png" alt="">
              <div class="product-label">
                <span class="new">NEW</span>
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Renovasi Ruang Tamu</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Arif Hidayat Setyanto</h3>
              </div>
              <h4 class="product-price">Start from 9 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/renovasiRuangTamu">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
        </div>

        <!--Product6-->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass2.png" alt="">
              <div class="product-label">
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Renovasi Ruang Makan</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Nadia Ningsih Juliawanti</h3>
              </div>
              <h4 class="product-price">Start from 9 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/renovasiRuangMakan">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
          <br><br><br>
        </div>

        <!--Product7-->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass4.png" alt="">
              <div class="product-label">
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Konstruksi Rumah Sakit</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Rahma Hadi wijaya</h3>
              </div>
              <h4 class="product-price">Start from 10 M</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/konstruksiRumahSakit">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
          <br><br><br>
        </div>

        <!--Product8-->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass4.png" alt="">
              <div class="product-label">
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Konstruksi Restoran</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Arif Hidayat Setyanto</h3>
              </div>
              <h4 class="product-price">Start from 200 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/konstruksiRestoran">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
          <br><br><br>
        </div>

        <!--Product9-->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass2.png" alt="">
              <div class="product-label">
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Konstruksi Sekolah</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Arif Hidayat Setyanto</h3>
              </div>
              <h4 class="product-price">Start from 800 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/konstruksiSekolah">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
          <br><br><br>
        </div>

        <!--Product10-->
        <div class="col-md-4">
          <div class="product">
            <div class="product-img">
              <img src="<?php echo base_url();?>asset/ass2.png" alt="">
              <div class="product-label">
              </div>
            </div>
            <div class="product-body">
              <div class="col-6">
                <p class="product-category">Category</p>
                <h3 class="product-name">Renovasi Restoran</h3>
              </div>
              <div class="col-6">
                <p class="product-category">Project Officer</p>
                <h3 class="product-name">Teguh Riandi</h3>
              </div>
              <h4 class="product-price">Start from 10 Jt</h4>
            </div>
            <div class="add-to-cart">
              <button class="add-to-cart-btn">
                <a href="<?php echo base_url()?>user/renovasiRestoran">Lihat Lebih Lanjut</a>
              </button>
            </div>
          </div>
          <br><br><br>
        </div>

      </div>
    </div>
  </div>
<br><br><br><br><br><br>

<?php include "footer.php"?>
  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
    </body>
</html>

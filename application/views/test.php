<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    * {
      box-sizing: border-box;
    }

    body {
      background: #f2f2f2;
    }

    #myInput {
      background-image: url("searchicon.png");
      background-position: 10px 10px;
      background-repeat: no-repeat;
      width: 50%;
      font-size: 12px;
      padding: 12px 20px 12px 40px;
      border: 1px solid #ddd;
      display: block;
      margin: auto;
      margin-bottom: 20px;
      border: 2px solid #fed700;
      height: 40px;
      border-radius: 5px 5px 5px 5px;
      background-color: #f2f2f2;
    }

    #myTable {
      border-collapse: collapse;
      width: 100%;
      border: 1px solid #ddd;
      font-size: 14px;
      font-family: arial, sans-serif;
    }

    #myTable th,
    #myTable td {
      text-align: left;
      padding: 8px;

    }

    #myTable tr {
      border-bottom: 1px solid #ddd;
    }

    #myTable tr.header,
    #myTable tr:hover {
      background-color: #ddd;
      color: gray;
    }

    #wrapper {
      width: 700px;
      margin: 0 auto;
    }

    tr:nth-child(even) {
      background-color: #f2f2f2;
    }

    h3 {
      font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
      color: #fed700;
      text-align: center;
      margin: auto;
    }

    h4 {
      font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
      color: #fed700;
      text-align: center;
      margin-top: 2px;
    }

    #square {
      background-color: #f2f2f2;
      width: 100%;
      height: 100%;
      margin: 0 auto;
    }
  </style>
</head>

<body>
  <div id="square">
    <div id="wrapper">
      <img src="Logo.png" width="30%" height="30%" style="display: block; margin: auto;">

      <h3>Musketeer Minimarket</h3>
      <h4>Selamat Berbelanjas</h4>
      <input type="text" id="myInput" onkeyup="myFunction()" class="searchTerm" placeholder="What are you looking for?">
      <table id="myTable">
        <tr class="header">
          <th style="width:35%;">Name</th>
          <th style="width:20%;">Jenis</th>
          <th style="width:20%;">Netto</th>
          <th style="width:25%;">Harga</th>
        </tr>
        <?php
        use BorderCloud\SPARQL\SparqlClient;

        require_once('vendor/autoload.php');

        $fuseki_server = "http://localhost:3030"; // change this to your fuseki server address
        $fuseki_sparql_db = "minimarket"; // change this to your fuseki Sparql database
        $endpoint = $fuseki_server . "/" . $fuseki_sparql_db . "/query";

        $sc = new SparqlClient();
        $sc->setEndpointRead($endpoint);
        $q = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX : <http://www.semanticweb.org/user/ontologies/2019/5/untitled-ontology-17#>
        
         "; 

        $rows = $sc->query($q, 'rows');
        $err = $sc->getErrors();
        if ($err) {
          print_r($err);
          throw new Exception(print_r($err, true));
        }
        foreach ($rows["result"]["rows"] as $row) {
          echo"<tr>";
          foreach ($rows["result"]["variables"] as $variable) {
            echo "<td>$row[$variable]</td>";
          }
          echo "</tr>";
        }
        ?>
    </div>
  </div>
  </table>

  <script>
    function myFunction() {
      var input, filter, table, tr, td, i, txtValue;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          txtValue = td.textContent || td.innerText;
          if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
  </script>

</body>

</html>
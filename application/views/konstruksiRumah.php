<?php 
$this->load->library('session');
?>
<head>
  <title>Order Job</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="<?php echo base_url();?>css/style6.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/footer.css" rel="stylesheet" type="text/css">
</head>
<body>
<?php include "header.php" ?>

    <div class="main">
      <div class="row">
        <div class="col-12">
          <!-- Carousel---> 
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                  <div class="item active">
                    <img src="<?php echo base_url();?>asset/order2-1.jpg" alt="rumah">
                    <div class="carousel-caption">
                      <h3>Platinum Package</h3>
                    </div>
                  </div>

                  <div class="item">
                    <img src="<?php echo base_url();?>asset/order2-2.jpg" alt="gedung">
                    <div class="carousel-caption">
                      <h3>Gold Package</h3>
                    </div>
                  </div>
                
                  <div class="item">
                    <img src="<?php echo base_url();?>asset/order2-3.jpg" alt="gedung">
                    <div class="carousel-caption">
                      <h3>Silver Package</h3>
                    </div>
                  </div>
                </div>
          </div>
        </div>

        <div class="col-6">
            <br>
            <h3>PAKET KONSTRUKSI RUMAH</h3>
          </div>
          <div class="col-6">
            <div class="profile">
              <img src="<?php echo base_url();?>asset/contact2.png" alt="">
              <a>Muhammad Fariz</a>
            </div>
          </div>
          <hr class="new5">

          <div class="col-12" align="center">
            <form method="post" action="
              <?php if (isset($_SESSION['email'])): ?>
                <?php echo base_url()?>User/form_order
              <?php else: ?>   
                 <?php echo base_url()?>controller_user/Login
            <?php endif ?>
              ">
            <box>
              <label class="container">Platinum Package
                <input type="radio" checked="checked" name="jenis_order" value='KonsRumahPP'>
                <span class="checkmark"></span>
              </label> 
                  <h4 style="padding-left: 20px;">Estimasi Biaya 250 Jt</h4>
                  <p style="padding-left: 20px;">
                    - Luas bangunan 30m2 (2 lantai) <br>
                    - Materian Lantai homogeneus tile 60x60 <br>
                    - Plafon gypsum 9mm + rangka hollow <br>
                    - Lantai granit <br>
                    - Batu Alam Palimanan untuk dinding<br>
                    - Beton kolom Struktur utama <br>
                    - Batu Pondasi <br>
                    - Beton Plat Lantai 2 <br>
                    - Ring Balok lantai  1 dan 2 <br>
                    - Sloof Pagar lantai 2 <br>
                    - Cat Dinding <br>
                    - Instalasi Listrik <br>
                    - Kamar Mandi (Sanitasi Air, Keramik, Closet, Keran, Septiktank) <br>
                    - Pipa Air <br>
                    - List gypsum  <br>
                 </p>
            </box>
            <box>
              <label class="container">Gold Package
                <input type="radio" checked="checked" name="jenis_order" value='konsRumahGP'>
                <span class="checkmark"></span>
              </label> 
                  <h4 style="padding-left: 20px;">Estimasi Biaya 240 Jt</h4>
                  <p style="padding-left: 20px;"> 
                    - Luas Bangunan 30m2<br>
                    - Materian Lantai homogeneus tile 60x60 <br>
                    - Batu Alam Palimanan untuk dinding <br>
                    - Bata hebel dan Plesteran (Atap) <br>
                    - Beton kolom Struktur utama <br>
                    - Beton Plat Lantai <br>
                    - Batu Pontasi <br>
                    - Ring Balok <br>
                    - Jendela utama2x2 meter <br>
                    - Cat dinding <br>
                    - Instalasi Listrik <br>
                    - Kamar Mandi (Sanitasi Air, Keramik, Closet, Keran, Septiktank) <br>
                    - Pipa Air <br><br><br><br>
                 </p>
            </box>
            <box>
              <label class="container">Silver Package
                <input type="radio" checked="checked" name="jenis_order" value='konsRumahSP'>
                <span class="checkmark"></span>
              </label>
                  <h4 style="padding-left: 20px;">Estimasi Biaya 110 Jt</h4> 
                  <p style="padding-left: 20px;"> 
                  - Luas Bangunan 30m2<br>
                  - Materian Lantai homogeneus tile 60x60 <br>
                  - Batu Alam Palimanan untuk dinding <br>
                  - Bata hebel dan Plesteran (Atap) <br>
                  - Beton kolom Struktur utama <br>
                  - Beton Plat Lantai <br>
                  - Batu Pontasi <br>
                  - Ring Balok <br>
                  - Jendela utama2x2 meter <br>
                  - Cat dinding <br>
                  - Instalasi Listrik <br>
                  - Kamar Mandi (Sanitasi Air, Keramik, Closet, Keran, Septiktank) <br>
                  - Pipa Air <br><br><br><br>
                  </p>
            </box><br>
            </div>
          </div>
          <div class="col-12" align="center">
            <input type="submit" name="Order" value="Order" style="width: 70%;height: 50px;"></input>  
          </div>

      </div>  
    </div>

    <?php include "footer.php" ?>
</body>
<head>
  <title>Order Job</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style type="text/css">
	/* The container */
		.container {
		  display: block;
		  position: relative;
		  padding-left: 35px;
		  margin-bottom: 12px;
		  cursor: pointer;
		  font-size: 22px;
		  -webkit-user-select: none;
		  -moz-user-select: none;
		  -ms-user-select: none;
		  user-select: none;
		}

		/* Hide the browser's default radio button */
		.container input {
		  position: absolute;
		  opacity: 0;
		  cursor: pointer;
		}

		/* Create a custom radio button */
		.checkmark {
		  position: absolute;
		  top: 0;
		  left: 0;
		  height: 25px;
		  width: 25px;
		  background-color: #eee;
		  border-radius: 50%;
		}

		/* On mouse-over, add a grey background color */
		.container:hover input ~ .checkmark {
		  background-color: #ccc;
		}

		/* When the radio button is checked, add a blue background */
		.container input:checked ~ .checkmark {
		  background-color: #2196F3;
		}

		/* Create the indicator (the dot/circle - hidden when not checked) */
		.checkmark:after {
		  content: "";
		  position: absolute;
		  display: none;
		}

		/* Show the indicator (dot/circle) when checked */
		.container input:checked ~ .checkmark:after {
		  display: block;
		}

		/* Style the indicator (dot/circle) */
		.container .checkmark:after {
		 	top: 9px;
			left: 9px;
			width: 8px;
			height: 8px;
			border-radius: 50%;
			background: white;
		}
  </style>
</head>
<body>
	 
		<h3 align="center"> Insert data</h3>
		<?php
			if($this->uri->segment(2) == "inserted")
				echo '<p class"text-success"> Data Inserted</p>'; 		
		?>


		
		<form method="post" action="<?php echo base_url()?>User/form_order">
			<h3>First Name</h3>
			<label class="container">Roni
  				<input type="radio" checked="checked" name="user_name" value="Roni">
  				<span class="checkmark"></span>
			</label>
			<label class="container">Budi
  				<input type="radio" checked="checked" name="user_name" value="Budi">
  				<span class="checkmark"></span>
			</label>

			<label class="container">Kuncoro
  				<input type="radio" checked="checked" name="user_name" value="Budi">
  				<span class="checkmark"></span>
			</label>

			<h3>Tipe Order</h3>
			<label class="container">Platinum Package
  				<input type="radio" checked="checked" name="jenis_order" value='PlatinumPackage'>
  				<span class="checkmark"></span>
			</label>

			<label class="container">Gold Package
  				<input type="radio" checked="checked" name="jenis_order" value='GoldPackage'>
  				<span class="checkmark"></span>
			</label>

			<label class="container">Silver Package 
  				<input type="radio" checked="checked" name="jenis_order" value="Silver Package">
  				<span class="checkmark"></span>
			</label>
			
				<input type="submit" name="insert" value="insert" style="width: 100px;height: 30px;"></input>
    
				
</body>
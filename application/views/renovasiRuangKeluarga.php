<?php 
$this->load->library('session');
?>
<head>
  <title>Order Job</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="<?php echo base_url();?>css/style6.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/footer.css" rel="stylesheet" type="text/css">

</head>
<body>
  <?php include "header.php" ?>

    <div class="main">
      <div class="row">
        <div class="col-12">
          <!-- Carousel---> 
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                  <div class="item active">
                    <img src="<?php echo base_url();?>asset/kamar1.jpg" alt="rumah">
                    <div class="carousel-caption">
                      <h3>Platinum Package</h3>
                    </div>
                  </div>

                  <div class="item">
                    <img src="<?php echo base_url();?>asset/kamar2.jpg" alt="gedung">
                    <div class="carousel-caption">
                      <h3>Gold Package</h3>
                    </div>
                  </div>
                
                  <div class="item">
                    <img src="<?php echo base_url();?>asset/kamar3.jpg" alt="gedung">
                    <div class="carousel-caption">
                      <h3>Silver Package</h3>
                    </div>
                  </div>
                </div>
          </div>
        </div>

        <div class="col-6">
            <br>
            <h3>PAKET RENOVASI RUANG KELUARGA</h3>
          </div>
          <div class="col-6">
            <div class="profile">
              <img src="<?php echo base_url();?>asset/contact2.png" alt="">
              <a>Tryas Dharossa Putri</a>
            </div>
          </div>
          <hr class="new5">

          <div class="col-12" align="center">
            <form method="post" action="
              <?php if (isset($_SESSION['email'])): ?>
                <?php echo base_url()?>User/form_order
              <?php else: ?>   
                 <?php echo base_url()?>controller_user/Login
            <?php endif ?>
              ">
            <box>
              <label class="container">Platinum Package
                <input type="radio" checked="checked" name="jenis_order" value='ruangKeluargaPP'>
                <span class="checkmark"></span>
              </label>
                <h4 style="padding-left: 20px;">Estimasi Biaya 40 Jt</h4> 
                <p style="padding-left: 20px">
                  - 1 unit Corner sofa<br>
                  - 2 unit Armchair<br>
                  - 1 unit Meja<br>
                  - 6 unit Vas bunga<br>
                  - 1 unit Karpet<br>
                  - 1 unit TV<br>
                  - 1 unit Meja TV<br><br><br>
                </p>
                
            </box>
            <box>
              <label class="container">Silver Package
                <input type="radio" checked="checked" name="jenis_order" value='ruangKeluargaGP'>
                <span class="checkmark"></span>
              </label> 
                  <h4 style="padding-left: 20px;">Estimasi Biaya 30jt</h4>
                  <p style="padding-left: 20px;"> 
                     Kriteria : <br>
                    - 1 unit Sofa minimalis <br>
                    - 1 unit Meja<br>
                    - 5 unit Vas bunga<br>
                    - 1 unit Karpet<br>
                    - 1 unit TV<br>
                    - 1 unit Meja TV<br><br><br>
                 </p>
                 
            </box>
            <box>
              <label class="container">Platinum Package
                <input type="radio" checked="checked" name="jenis_order" value='ruangKeluargaSP'>
                <span class="checkmark"></span>
              </label> 
                  <h4 style="padding-left: 20px;">Estimasi Biaya 50 Jt</h4>
                  <p style="padding-left: 20px;"> 
                    Kriteria : <br>
                    - 1 unit Corner sofa <br>
                    - 4 unit Armchair<br>
                    - 2 unit Meja<br>
                    - 1 unit Vas bunga<br>
                    - 1 unit Karpet<br>
                    - 2 unit TV<br>
                    - 1 unit AC<br>
                    - 1 unit Meja TV<br>
                  </p>
                  
            </box><br>

            
          </div>
          <div class="col-12" align="center">
            <input type="submit" name="Order" value="Order" style="width: 70%;height: 50px;"></input>  
          </div>

      </div>  
    </div>

    <?php include "footer.php" ?>
</body>
<div class="navbar">
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
      <div class="row">
        <div class="col-8" style="padding-left: 10px">
          <div class="logo">
            <a href="<?php echo base_url()?>user/home"><img src="<?php echo base_url();?>asset/logo.png" style="width:40px"></a>
          </div>
          <div class="right-section">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="padding-top: 3px">Service <span class="caret"></span></a>
                <ul class="dropdown-menu" style="margin-left: 5%;">
                  <li><a href="<?php echo base_url()?>user/KonstruksiRumah">Konstruksi Rumah</a></li>
                  <li><a href="<?php echo base_url()?>user/renovasiDapur">Renovasi Dapur</a></li>
                  <li><a href="<?php echo base_url()?>user/renovasiKamar">Renovasi Kamar Tidur</a></li>
                  <li><a href="<?php echo base_url()?>user/renovasiRuangKeluarga">Renovasi Ruang Keluarga</a></li>
                  <li><a href="<?php echo base_url()?>user/renovasiRuangTamu">Renovasi Ruang Tamu</a></li>
                  <li><a href="<?php echo base_url()?>user/renovasiRuangMakan">Renovasi Ruang Makan</a></li>
                  <li><a href="<?php echo base_url()?>user/renovasiRestoran">Renovasi Restoran</a></li>
                  <li><a href="<?php echo base_url()?>user/KonstruksiRumahSakit">Konstruksi Rumah Sakit</a></li>
                  <li><a href="<?php echo base_url()?>user/KonstruksiRestoran">Konstruksi Restoran</a></li>
                  <li><a href="<?php echo base_url()?>user/KonstruksiSekolah">Konstruksi Sekolah</a></li>
                </ul>
              <a href="<?php echo base_url()?>user/infoDashboard" style="padding-top:3px">Information</a>
              <a href="<?php echo base_url()?>user/contacts" style="padding-top:3px">Contact Us</a>
          </div>
        </div>

        <div class="col-4">
          <div class="login">
            <?php if (isset($_SESSION['email'])): ?>
                 <button><a class="right" href="<?php echo base_url();?>controller_user/Logout">Logout</a></button>
              <?php else: ?>   
              <button onclick="window.location.replace('<?php echo base_url();?>controller_user/Register');"><span class="glyphicon glyphicon-user"></span> Sign Up</button>
              <button onclick="window.location.replace('<?php echo base_url();?>controller_user/Login');"><span class="glyphicon glyphicon-log-in"></span> Login</button>
            <?php endif ?>
          </div>
        </div>      
      </div>
    </div>
    </nav>
  </div>
<?php 
$this->load->library('session');
?>
<head>
  <title>Order Job</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="<?php echo base_url();?>css/style6.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/footer.css" rel="stylesheet" type="text/css">

</head>
<body>
  <?php include "header.php" ?>

    <div class="main">
      <div class="row">
        <div class="col-12">
          <!-- Carousel---> 
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                  <div class="item active">
                    <img src="<?php echo base_url();?>asset/ruangmakan1.jpg" alt="rumah">
                    <div class="carousel-caption">
                      <h3>Platinum Package</h3>
                    </div>
                  </div>

                  <div class="item">
                    <img src="<?php echo base_url();?>asset/ruangmakan2.jpg" alt="gedung">
                    <div class="carousel-caption">
                      <h3>Gold Package</h3>
                    </div>
                  </div>
                
                  <div class="item">
                    <img src="<?php echo base_url();?>asset/ruangmakan3.jpg" alt="gedung">
                    <div class="carousel-caption">
                      <h3>Silver Package</h3>
                    </div>
                  </div>
                </div>
          </div>
        </div>

        <div class="col-6">
            <br>
            <h3>PAKET RENOVASI RUANG MAKAN</h3>
          </div>
          <div class="col-6">
            <div class="profile">
              <img src="<?php echo base_url();?>asset/contact2.png" alt="">
              <a>Nadi Ningsih Juliawanti</a>
            </div>
          </div>
          <hr class="new5">

          <div class="col-12" align="center">
            <form method="post" action="
              <?php if (isset($_SESSION['email'])): ?>
                <?php echo base_url()?>User/form_order
              <?php else: ?>   
                 <?php echo base_url()?>controller_user/Login
            <?php endif ?>
              ">
            <box>
              <label class="container">Silver Package
                <input type="radio" checked="checked" name="jenis_order" value='ruangMakanPP'>
                <span class="checkmark"></span>
              </label>
                <h4 style="padding-left: 20px;">Estimasi Biaya 25 Jt</h4> 
                <p style="padding-left: 20px">
                Kriteria : <br>
                - 1 unit meja makan untuk 4 orang<br>
                - 4 unit kursi<br>
                - 1 taplak meja<br>
                - 1 unit lemari makan<br>
                - 1 unit lampu <br>
                - 2 set perlengkapan makan<br><br><br><br><br>
                </p>
               
            </box>
            <box>
              <label class="container">Gold Package
                <input type="radio" checked="checked" name="jenis_order" value='ruangMakanGP'>
                <span class="checkmark"></span>
              </label> 
                  <h4 style="padding-left: 20px;">Estimasi Biaya 10 Jt</h4>
                  <p style="padding-left: 20px;"> 
                     Kriteria : <br>
                    - 1 unit meja makan untuk 6 orang<br>
                    - 6 unit kursi<br>
                    - 1 taplak meja<br>
                    - 1 unit lemari makan<br>
                    - 2 unit lampu <br>
                    - 4 set perlengkapan makan<br>
                    - 1 unit wastafel<br>
                    - 1 unit coffee maker<br><br><br>
                 </p>
                 
            </box>
            <box>
              <label class="container">Platinum Package
                <input type="radio" checked="checked" name="jenis_order" value='ruangMakanSP'>
                <span class="checkmark"></span>
              </label> 
                  <h4 style="padding-left: 20px;">Estimasi Biaya 6,5 Jt</h4>
                  <p style="padding-left: 20px;"> 
                    Kriteria : <br>
                    - 1 unit meja makan untuk 6 orang<br>
                    - 6 unit kursi<br>
                    - 1 taplak meja<br>
                    - 1 unit lemari makan<br>
                    - 3 unit lampu <br>
                    - 6 set perlengkapan makan<br>
                    - 1 unit wastafel<br>
                    - 1 unit kulkas<br>
                    - 1 unit coffee maker<br>
                    - 1 unit toaster<br>
                  </p>
                  
            </box><br>
            
          </div>
          <div class="col-12" align="center">
            <input type="submit" name="Order" value="Order" style="width: 70%;height: 50px;"></input>  
          </div>

      </div>  
    </div>

    <?php include "footer.php" ?>
</body>
<?php 
$this->load->library('session');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Konstan</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <!-- Google font -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

  <!-- Slick -->
  <link type="text/css" rel="stylesheet" href="css/slick.css"/>
  <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

  <!-- nouislider -->
  <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

  <!-- Font Awesome Icon -->
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="<?php echo base_url();?>css/style6.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/footer.css" rel="stylesheet" type="text/css">
  <!-- <link href="css/stylee.css" rel="stylesheet" type="text/css"> -->
</head>
<body>
  <?php include "header.php" ?>

<div class="main" style="margin-top: 50px;text-align: center">
<div class="row">
  <div class="col-12">  
    <div class="contact">
      <h2>Any question? please contact to our consultant</h2>
      <box>
        <img src="<?php echo base_url()?>asset/contact2.png"></img>
        <br><br><h3>Teguh Riandi</h3>
        <p>Interior Designer</p>
        <p>WhatsApp : 0811 2333 1233</p>
        <p>Email : teguhriandi@yahoo.com</p><br>
      </box>
      <box>
        <img src="<?php echo base_url()?>asset/contact2.png"></img>
        <br><br><h3>Muhammad Fariz</h3>
        <p>Kontraktor</p>
        <p>WhatsApp : 0812 1231 1222</p>
        <p>Email : MFariz01@yahoo.com</p><br>
      </box>
      <box>
        <img src="<?php echo base_url()?>asset/contact2.png"></img>
        <br><br><h3>Yusuf Alfianto</h3>
        <p>Interior Designer</p>
        <p>WhatsApp : 0821 2821 3532</p>
        <p>Email : MYusufAlf02@yahoo.com</p><br>
      </box>
      <box>
        <img src="<?php echo base_url()?>asset/contact.png"></img>
        <br><br><h3>Triyas Dharosa Putri </h3>
        <p>Interior Designer</p>
        <p>WhatsApp : 0811 6882 0442</p>
        <p>Email : Triyasdharosa@gmail.com</p><br>
      </box>
      <box>
        <img src="<?php echo base_url()?>asset/contact2.png"></img>
        <br><br><h3>Arif Hidayat Setyanto</h3>
        <p>Kontraktor</p>
        <p>WhatsApp : 0812 9572 7592</p>
        <p>Email : arfhdyt@gmail.com</p><br>
      </box>
      <box>
        <img src="<?php echo base_url()?>asset/contact2.png"></img>
        <br><br><h3>Fajar Refo Pradana</h3>
        <p>Kontraktor</p>
        <p>WhatsApp : 0821 2221 3332</p>
        <p>Email : refofajar@gmail.com</p><br>
      </box>
      <box>
        <img src="<?php echo base_url()?>asset/contact2.png"></img>
        <br><br><h3>Rhama Hadiwijaya</h3>
        <p>Kontraktor</p>
        <p>WhatsApp : 0852 7539 6593</p>
        <p>Email : rhamahadiwijaya@gmail.com</p><br>
      </box>
      <box>
        <img src="<?php echo base_url()?>asset/contact.png"></img>
        <br><br><h3>Nadia Ningsih J</h3>
        <p>Interior Designer</p>
        <p>WhatsApp : 0891 6380 6382</p>
        <p>Email : nading1252@gmail.com</p><br>
      </box>
      <box>
        <img src="<?php echo base_url()?>asset/contact2.png"></img>
        <br><br><h3>Ikhsan Maulana</h3>
        <p>Interior Designer</p>
        <p>WhatsApp : 0821 7683 5492</p>
        <p>Email : ikhsangokil@gmail.com</p><br>
      </box>
    </div>  
  </div>
</div>
</div>

<?php include "footer.php" ?>


  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
    </body>
</html>


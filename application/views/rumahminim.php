<?php 
$this->load->library('session');
?>
<head>
  <title>Order Job</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/font-awesome.min.css">
  <link href="<?php echo base_url();?>css/style6.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/style7.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url();?>css/footer.css" rel="stylesheet" type="text/css">

</head>
<body>
  <?php include "header.php" ?>

    <div class="main">
      <div class="row">
        <div class="col-12">
          <!-- Carousel---> 
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                  <div class="item active">
                    <img src="<?php echo base_url();?>asset/minimalis.jpg">
                    <div class="carousel-caption">
                    </div>
                  </div>
                </div>
          </div>
        </div>

          <div class="col-12">
            <br><br>
            <h2 style="text-align: left">Mengapa Rumah Minimalis ?</h2>
            <p>Beragam desain rumah dapat menjadi impian setiap orang. Ada yang ingin tinggal di sebuah rumah besar dengan halaman luas bak kastil kuno. Ada juga yang hanya menginginkan desain rumah putih minimalis yang sederhana. Jika Anda menginginkan desain rumah minimalis yang terlihat simpel, mungkin Anda patut menyimak rancangan rumah putih sederhana karya arsitek Sontang M. Siregar ini. Rumah minimalis ini diberi nama R House at Taman Laguna. Berada di Cibubur, Jakarta, rumah tinggal yang selesai dibangun pada tahun 2016 ini didirikan di atas lahan seluas 144 meter persegi. Dengan luas total bangunan 100 meter persegi, rumah kotak putih ini memang tampak sederhana dan simpel dari luar. Namun, itu sama sekali tak mengurangi kreativitas sang desainer untuk menciptakan hunian idaman. Tak percaya? Mari kita menyusuri satu persatu ruangan, bagian demi bagian rumah minimalis nan cantik ini!

Artikel ini telah tayang di Kompas.com dengan judul "Ada Banyak Kejutan di Rumah Minimalis Ini", https://properti.kompas.com/read/2019/06/14/140843921/ada-banyak-kejutan-di-rumah-minimalis-ini?page=all.

Editor : Hilda B Alexander</p>
          </div>
          

      </div>  
    </div>

    <?php include "footer.php" ?>
</body>
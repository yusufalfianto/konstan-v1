<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function home(){
    $this->load->view('home');
  }

  public function konstruksiRumah(){
    // load view
    $this->load->view('konstruksiRumah');
  }

  public function renovasiDapur(){
    // load view
    $this->load->view('renovasiDapur');
  }
  
  public function renovasiKamar(){
    // load view
    $this->load->view('renovasiKamar');
  }
  public function renovasiRuangKeluarga(){
    // load view
    $this->load->view('renovasiRuangKeluarga');
  }
  public function renovasiRuangTamu(){
    // load view
    $this->load->view('renovasiRuangTamu');
  }
  public function renovasiRuangMakan(){
    // load view
    $this->load->view('renovasiRuangMakan');
  }
  public function renovasiRestoran(){
    // load view
    $this->load->view('renovasiRestoran');
  }
  public function konstruksiRestoran(){
    // load view
    $this->load->view('konstruksiRestoran');
  }
  public function konstruksiSekolah(){
    // load view
    $this->load->view('konstruksiSekolah');
  }
  public function konstruksiRumahSakit(){
    // load view
    $this->load->view('konstruksiRumahSakit');
  }
  public function contacts(){
    // load view
    $this->load->view('contacts');
  }

  public function infoDashboard(){
    // load view
    $this->load->view('infoDashboard');
  }

  public function rumahminim(){
    // load view
    $this->load->view('rumahminim');
  }

  public function form_order(){
    $this->load->library('form_validation');
    $this->load->library('session');  
        //  $this->form_validation->set_rules("user_name", "User Name", 'required|alpha');  
        $this->form_validation->set_rules("jenis_order", "Jenis Order", 'required|alpha');  
          if($this->form_validation->run()){
            //true
            $this->load->model("user_model");  
            
            $data = array(  
                    // "user_name"     =>$this->input->post("user_name"),  
                     "jenis_order"   =>$this->input->post("jenis_order"),
                     "email"         => $_SESSION['email']
            ); 
            
            $this->user_model->insert_data($data);
            redirect (base_url() . "User/inserted") ; 

          }

  
           else{
                $this->index();
           }
  }

  public function inserted(){
    $this->load->view('home');
  }    

}